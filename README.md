# Json Schema Editor

This application tries to ease the creation and modification of JSON schemas.

## Run

### From Source

Execute `gradlew run`

### From Intellij

Create a new 'Application' configuration with the following parameters:

 * Main class: `com.daslernen.jsonschemaeditor.JsonSchemaEditor`