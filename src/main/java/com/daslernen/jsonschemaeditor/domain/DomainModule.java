package com.daslernen.jsonschemaeditor.domain;

import com.daslernen.jsonschemaeditor.annotation.Json;
import com.daslernen.jsonschemaeditor.annotation.Schema;
import com.daslernen.jsonschemaeditor.configuration.EditorConfiguration;
import com.daslernen.jsonschemaeditor.service.FileService;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;

import java.text.NumberFormat;

public final class DomainModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(NumberFormat.class).toProvider(NumberFormat::getPercentInstance);
    }

    @Json
    @Provides
    public DocumentHandler createJsonDocumentHandler(@Json final EditorConfiguration configuration,
                                                 final DialogProvider dialogProvider,
                                                 final FileService fileService) {
        return new DocumentHandler(configuration, dialogProvider, fileService);
    }

    @Schema
    @Provides
    public DocumentHandler createSchemaDocumentHandler(@Schema final EditorConfiguration configuration,
                                                 final DialogProvider dialogProvider,
                                                 final FileService fileService) {
        return new DocumentHandler(configuration, dialogProvider, fileService);
    }
}
