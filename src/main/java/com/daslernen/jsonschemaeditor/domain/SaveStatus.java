package com.daslernen.jsonschemaeditor.domain;

public enum SaveStatus {
    SUCCESS,
    FAILURE,
    CANCEL
}
