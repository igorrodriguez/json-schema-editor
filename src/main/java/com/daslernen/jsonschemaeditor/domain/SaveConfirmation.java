package com.daslernen.jsonschemaeditor.domain;

public enum SaveConfirmation {
    SAVE,
    DO_NOT_SAVE,
    CANCEL
}
