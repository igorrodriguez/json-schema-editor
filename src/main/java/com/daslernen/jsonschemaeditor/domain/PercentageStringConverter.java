package com.daslernen.jsonschemaeditor.domain;

import com.google.inject.Provider;
import javafx.util.StringConverter;
import javaslang.control.Try;

import javax.inject.Inject;
import java.text.NumberFormat;

public final class PercentageStringConverter extends StringConverter<Number> {

    private final Provider<NumberFormat> formatterProvider;

    @Inject
    public PercentageStringConverter(final Provider<NumberFormat> formatterProvider) {
        this.formatterProvider = formatterProvider;
    }

    @Override
    public String toString(final Number value) {
        final NumberFormat formatter = formatterProvider.get();
        formatter.setMaximumFractionDigits(0);
        formatter.setMinimumFractionDigits(0);
        formatter.setParseIntegerOnly(true);
        return formatter.format(value.doubleValue() / 100);
    }

    @Override
    public Number fromString(final String stringValue) {
        final NumberFormat formatter = formatterProvider.get();
        return Try.of(() -> formatter.parse(stringValue))
                .getOrElseThrow(e -> new IllegalAccessError(String.format("Value [%s] is not a valid number", stringValue)));
    }
}
