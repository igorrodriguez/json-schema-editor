package com.daslernen.jsonschemaeditor.domain;

import javafx.util.StringConverter;

import javax.inject.Inject;

public final class PercentageDoubleStringConverter extends StringConverter<Double> {

    private final PercentageStringConverter delegate;

    @Inject
    public PercentageDoubleStringConverter(final PercentageStringConverter percentageStringConverter) {
        this.delegate = percentageStringConverter;
    }

    @Override
    public String toString(final Double value) {
        return delegate.toString(value);
    }
    @Override
    public Double fromString(final String string) {
        return delegate.fromString(string).doubleValue();
    }
}
