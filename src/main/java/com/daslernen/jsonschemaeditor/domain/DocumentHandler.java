package com.daslernen.jsonschemaeditor.domain;

import com.daslernen.jsonschemaeditor.configuration.EditorConfiguration;
import com.daslernen.jsonschemaeditor.configuration.OpenFile;
import com.daslernen.jsonschemaeditor.service.FileService;

import java.nio.file.Path;
import java.util.Optional;

import static java.lang.String.format;

public final class DocumentHandler {

    private final EditorConfiguration configuration;
    private final DialogProvider dialogProvider;
    private final FileService fileService;

    private final DocumentStateHolder stateHolder = new DocumentStateHolder();

    public DocumentHandler(final EditorConfiguration configuration, final DialogProvider dialogProvider, final FileService fileService) {
        this.configuration = configuration;
        this.dialogProvider = dialogProvider;
        this.fileService = fileService;
        stateHolder.addStateChangeListener(this::updateOpenFileInConfiguration);
    }

    public Optional<String> openLastFile() {
        return configuration.getOpenFile().getPath()
                .filter(fileService::exists)
                .flatMap(this::loadFile);
    }

    public Optional<String> openFile(final String editorContent) {
        final SaveStatus saveStatus = saveFile(editorContent, true);
        if (saveStatus == SaveStatus.SUCCESS) {
            return dialogProvider.showOpenDialog(configuration.getLastOpenDirectory())
                    .flatMap(this::loadFile);
        }
        return Optional.empty();
    }

    public SaveStatus closeFile(final String editorContent) {
        final SaveStatus saveStatus = saveFile(editorContent, true);
        if (saveStatus == SaveStatus.SUCCESS) {
            stateHolder.updateToNone();
        }
        return saveStatus;
    }

    public void saveFileWithoutConfirmation(final String content) {
        saveFile(content, false);
    }

    private Optional<String> loadFile(final Path path) {
        configuration.setLastOpenDirectory(path.getParent());
        return fileService.readString(path)
                .onSuccess(text -> updateState(path, text))
                .onFailure(e -> dialogProvider.showErrorAlert("Failed to open the file", format("Couldn't open the file [%s]", path), e))
                .toJavaOptional();
    }

    private SaveStatus saveFile(final String content, final boolean requestUserConfirmation) {
        final Optional<Path> openFileOption = configuration.getOpenFile().getPath();
        if (openFileOption.isPresent()) {
            return saveExistingFile(openFileOption.get(), content, requestUserConfirmation);
        } else if (requiresCreatingNewFile(content, requestUserConfirmation)) {
            return saveNewFile(content);
        }
        return SaveStatus.SUCCESS;
    }

    private boolean requiresCreatingNewFile(final String content, final boolean requestUserConfirmation) {
        final boolean hasEmptySpacesButTheUserDidNotExplicitlySaveIt = requestUserConfirmation && content.trim().isEmpty();
        return !hasEmptySpacesButTheUserDidNotExplicitlySaveIt;
    }

    private SaveStatus saveExistingFile(final Path path, final String content, final boolean requestUserConfirmation) {
        if (contentHasChanged(content)) {
            return saveForConfirmation(path, content, confirm(requestUserConfirmation));
        }
        return SaveStatus.SUCCESS;
    }

    private SaveStatus saveNewFile(final String content) {
        final SaveConfirmation saveConfirmation = askUserConfirmation();
        if (saveConfirmation == SaveConfirmation.SAVE) {
            return dialogProvider.showSaveDialog(configuration.getLastOpenDirectory())
                    .map(selectedPath -> saveFile(selectedPath, content))
                    .orElse(SaveStatus.CANCEL);
        } else if (saveConfirmation == SaveConfirmation.CANCEL) {
            return SaveStatus.CANCEL;
        }
        return SaveStatus.SUCCESS;
    }

    private SaveConfirmation confirm(final boolean confirm) {
        if (!confirm) {
            return SaveConfirmation.SAVE;
        }
        return askUserConfirmation();
    }

    private SaveConfirmation askUserConfirmation() {
        final String message = "The content of the file changed, do you want to save it?";
        return dialogProvider.showSaveConfirmationAlert("Save changes?", message);
    }

    private boolean contentHasChanged(String content) {
        return !updateNewLineCharacters(content).equals(getState().getContent());
    }

    private SaveStatus saveForConfirmation(final Path path, final String content, final SaveConfirmation confirmationResponse) {
        switch (confirmationResponse) {
            case SAVE:
                return saveFile(path, content);
            case DO_NOT_SAVE:
                return SaveStatus.SUCCESS;
            case CANCEL:
                return SaveStatus.CANCEL;
            default:
                throw new UnsupportedOperationException(format("Cannot handle response with value [%s]", confirmationResponse));
        }
    }

    private SaveStatus saveFile(final Path path, final String content) {
        final String fileContent = updateNewLineCharacters(content);
        return fileService.save(path, fileContent)
                .onSuccess(filePath -> updateState(filePath, fileContent))
                .onFailure(e -> dialogProvider.showErrorAlert("Failed to save the file", format("Couldn't save the file [%s]", path), e))
                .map(p -> SaveStatus.SUCCESS)
                .getOrElse(SaveStatus.FAILURE);
    }

    private void updateState(final Path path, final String savedContent) {
        stateHolder.updateState(path, savedContent);
    }

    private String updateNewLineCharacters(final String content) {
        final NewLineStyle newLineStyle = getState().getNewLineStyle();
        return content.replaceAll("\\R", newLineStyle.getValue());
    }

    private void updateOpenFileInConfiguration(final CurrentFile currentFile) {
        final OpenFile openFile = new OpenFile()
                .setPath(currentFile.getPath())
                .setContent(currentFile.getContent())
                .setNewLineStyle(currentFile.getNewLineStyle());
        configuration.setOpenFile(openFile);
    }

    private CurrentFile getState() {
        return stateHolder.getState();
    }
}
