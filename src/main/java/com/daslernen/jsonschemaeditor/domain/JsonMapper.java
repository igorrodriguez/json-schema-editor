package com.daslernen.jsonschemaeditor.domain;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.google.inject.Singleton;
import javaslang.control.Try;

import java.io.IOException;
import java.nio.file.Path;

@Singleton
public class JsonMapper {

    private static final ObjectMapper MAPPER = new ObjectMapper()
            .enable(SerializationFeature.INDENT_OUTPUT)
            .registerModule(new Jdk8Module());

    public <T> Try<T> readFromFile(final Path path, final Class<T> type) {
        return Try.of(() -> MAPPER.readValue(path.toFile(), type));
    }

    public Try<Void> writeToFile(final Path path, final Object object) {
        return Try.run(() -> MAPPER.writeValue(path.toFile(), object));
    }

    public JsonNode nodeFromString(final String jsonString) throws IOException {
        return MAPPER.readTree(jsonString);
    }
}
