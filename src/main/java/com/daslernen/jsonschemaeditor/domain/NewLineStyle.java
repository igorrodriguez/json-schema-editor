package com.daslernen.jsonschemaeditor.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class NewLineStyle {

    public static final NewLineStyle SYSTEM_DEFAULT_STYLE = new NewLineStyle(System.lineSeparator());

    private static final Pattern NEW_LINE_PATTERN = Pattern.compile(".*(\\R).*");

    private final String value;

    @JsonCreator
    public NewLineStyle(@JsonProperty("value") final String value) {
        Preconditions.checkNotNull(value);
        this.value = value;
    }

    public static NewLineStyle findFromText(final String text) {
        final Matcher matcher = NEW_LINE_PATTERN.matcher(text);
        if (matcher.matches()) {
            return new NewLineStyle(matcher.group(1));
        }
        return SYSTEM_DEFAULT_STYLE;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof NewLineStyle))
            return false;
        NewLineStyle that = (NewLineStyle) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        final String printableValue = value.equals("\n") ? "\\n" : value.equals("\r\n") ? "\\r\\n" : "cannot display";
        return MoreObjects.toStringHelper(this).add("value", printableValue).toString();
    }
}