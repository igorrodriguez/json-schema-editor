package com.daslernen.jsonschemaeditor.domain;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

import static com.daslernen.jsonschemaeditor.domain.NewLineStyle.findFromText;

public final class CurrentFile {

    public static final CurrentFile NONE = new CurrentFile(Paths.get("."), null, NewLineStyle.SYSTEM_DEFAULT_STYLE);

    private final Path path;
    private final String originalContent;
    private final NewLineStyle originalLineStyle;

    public CurrentFile(final Path path, final String originalContent) {
        this(path, originalContent, findFromText(originalContent));
    }

    CurrentFile(final Path path, final String originalContent, final NewLineStyle originalLineStyle) {
        Preconditions.checkNotNull(path);

        this.path = path;
        this.originalContent = originalContent;
        this.originalLineStyle = originalLineStyle;
    }

    public Path getPath() {
        return path;
    }

    public String getContent() {
        return originalContent;
    }

    public NewLineStyle getNewLineStyle() {
        return originalLineStyle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof CurrentFile))
            return false;
        CurrentFile currentFile = (CurrentFile) o;
        return Objects.equals(path, currentFile.path) &&
                Objects.equals(originalContent, currentFile.originalContent) &&
                Objects.equals(originalLineStyle, currentFile.originalLineStyle);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path, originalContent, originalLineStyle);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("path", path)
                .add("originalContent", originalContent)
                .add("originalLineStyle", originalLineStyle)
                .toString();
    }
}