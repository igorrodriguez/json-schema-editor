package com.daslernen.jsonschemaeditor.domain;

import java.nio.file.Path;
import java.util.Optional;

public interface DialogProvider {

    Optional<Path> showOpenDialog(Path initialDirectory);

    Optional<Path> showSaveDialog(Path initialDirectory);

    void showErrorAlert(String title, String message, Throwable exception);

    SaveConfirmation showSaveConfirmationAlert(String title, String message);
}
