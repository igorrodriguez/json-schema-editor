package com.daslernen.jsonschemaeditor.validation;

import com.fasterxml.jackson.core.JsonLocation;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

import java.util.Objects;

import static com.daslernen.jsonschemaeditor.validation.ValidationStatus.ERROR;
import static com.daslernen.jsonschemaeditor.validation.ValidationStatus.OK;
import static com.daslernen.jsonschemaeditor.validation.ValidationStatus.WARNING;
import static java.lang.String.format;

public final class ValidationMessage {

    private static final String OK_MESSAGE_TEXT = "Valid";
    private static final ValidationMessage OK_MESSAGE = new ValidationMessage(OK, OK_MESSAGE_TEXT);

    private final ValidationStatus status;
    private final String text;

    public static ValidationMessage ok() {
        return OK_MESSAGE;
    }

    public static ValidationMessage warn(final String message) {
        Preconditions.checkNotNull(message);
        return new ValidationMessage(WARNING, message);
    }

    public static ValidationMessage error(final String message) {
        Preconditions.checkNotNull(message);
        return new ValidationMessage(ERROR, message);
    }

    public static ValidationMessage jsonError(final JsonLocation location, final String message) {
        Preconditions.checkNotNull(message);
        return error(format("(%d:%d) %s", location.getLineNr(), location.getColumnNr(), message));
    }

    public ValidationMessage(final ValidationStatus status, final String text) {
        Preconditions.checkNotNull(status);

        this.status = status;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public ValidationStatus getStatus() {
        return status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof ValidationMessage))
            return false;
        ValidationMessage that = (ValidationMessage) o;
        return status == that.status && Objects.equals(text, that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status, text);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("status", status).add("text", text).toString();
    }
}
