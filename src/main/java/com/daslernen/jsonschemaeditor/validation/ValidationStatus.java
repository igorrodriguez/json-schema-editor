package com.daslernen.jsonschemaeditor.validation;

import java.util.Comparator;

import static java.lang.Integer.compare;

public enum ValidationStatus {

    OK(30),
    WARNING(20),
    ERROR(10);

    private static final Comparator<ValidationStatus> DESCENDING_LOG_LEVEL_COMPARATOR =
            (val1, val2) -> compare(val1.importance, val2.importance);

    private final int importance;

    /**
     * @param importance A number used to sort instances by how serious they are, where lower means more important.
     */
    ValidationStatus(final int importance) {
        this.importance = importance;
    }

    public static Comparator<ValidationStatus> descendingLogLevelComparator() {
        return DESCENDING_LOG_LEVEL_COMPARATOR;
    }
}
