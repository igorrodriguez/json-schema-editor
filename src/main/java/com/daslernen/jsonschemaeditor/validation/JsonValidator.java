package com.daslernen.jsonschemaeditor.validation;

import com.daslernen.jsonschemaeditor.domain.JsonMapper;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.ImmutableMap;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Pattern;

import static com.daslernen.jsonschemaeditor.validation.ValidationMessage.jsonError;
import static com.daslernen.jsonschemaeditor.validation.ValidationVisitor.Result.*;
import static java.lang.String.format;
import static java.util.regex.Pattern.compile;

//import static com.daslernen.jsonschemaeditor.util.JsonUtil.getMapper;

public class JsonValidator implements Validator {

    private final Map<Pattern, Function<JsonParseException, ValidationMessage>> errorMapper =
            ImmutableMap.<Pattern, Function<JsonParseException, ValidationMessage>>builder()
                    .put(compile("Unexpected end-of-input: expected close marker for OBJECT"), this::unclosedObject)
                    .put(compile("Unexpected close marker '}': expected ']' \\(for ROOT starting at"), this::unopenedObject)
                    .put(compile("Unexpected character \\('.' \\(code \\d+\\)\\): was expecting double-quote to start field name"), this::missingStartQuoteOnPropertyName)
                    .put(compile("Unexpected end-of-input: was expecting closing '\"' for name"), this::missingEndQuoteOnPropertyName)
                    .put(compile("Unexpected character \\('.' \\(code \\d+\\)\\): was expecting a colon to separate field name and value"), this::missingColon)
                    .put(compile("Unexpected character \\('}' \\(code 125\\)\\): expected a valid value \\(number, String, array, object, 'true', 'false' or 'null'\\)"), this::missingPropertyValue)
                    .put(compile("Unrecognized token '.': was expecting \\('true', 'false' or 'null'\\)"), this::invalidPropertyValue)
                    .put(compile("Unrecognized token '.+': was expecting 'null', 'true', 'false' or NaN"), this::invalidPropertyValue)
                    .put(compile("Unexpected character \\('.' \\(code \\d+\\)\\): expected a valid value \\(number, String, array, object, 'true', 'false' or 'null'\\)"), this::invalidPropertyValue)
                    .put(compile("Unexpected end-of-input: was expecting closing quote for a string value"), this::missingEndQuoteOnPropertyValue)
                    .put(compile("Unexpected close marker '}': expected ']' \\(for ARRAY starting"), this::unclosedArray)
                    .put(compile("Unexpected character \\(']' \\(code \\d+\\)\\): expected a value"), this::redundantCommaInArray)
                    .put(compile("Unexpected end-of-input in FIELD_NAME"), this::emptyPropertyName)
                    .build();
    private final JsonMapper jsonMapper;

    @Inject
    public JsonValidator(final JsonMapper jsonMapper) {
        this.jsonMapper = jsonMapper;
    }

    @Override
    public ValidationMessage validate(final RawDocument jsonDocument) {
        return validateJson(jsonDocument)
                .getMessage();
    }

    final ValidationVisitor<JsonNode> validateJson(final RawDocument jsonDocument) {
        return ValidationVisitor.create(() -> jsonStringIsNotEmpty(jsonDocument))
                .map(this::toJsonNodeDocument)
                .map(this::getValidatableNode);
    }

    private ValidationVisitor.Result<JsonNode> getValidatableNode(final Document<JsonNode> jsonDocument) {
        final JsonPointer pathToRoot = jsonDocument.getPathToRoot();
        try {
            final JsonNode rootNode = jsonDocument.getValue().at(pathToRoot);
            if (!rootNode.isMissingNode()) {
                return okResult(rootNode);
            }
        } catch (IllegalArgumentException e) {
            // Default value returned at end of method
        }
        return errorResult(format("Path [%s] doesn't point to a valid json", pathToRoot));
    }

    private ValidationVisitor.Result<Document<JsonNode>> toJsonNodeDocument(final RawDocument jsonDocument) {
        final String jsonString = jsonDocument.getValue();
        final JsonNode json;
        try {
            json = jsonMapper.nodeFromString(jsonString);
        } catch (JsonParseException e) {
            return result(getError(e));
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Failed to deserialise jsonDocument [%s]", jsonString));
        }

        final JsonPointer pathToRoot;
        try {
            pathToRoot = JsonPointer.compile(jsonDocument.getPathToRoot());
        } catch (IllegalArgumentException e) {
            return errorResult(format("Value [%s] is not a valid json pointer path", jsonDocument.getPathToRoot()));
        }
        return okResult(new Document<>(json, pathToRoot));
    }

    private ValidationVisitor.Result<RawDocument> jsonStringIsNotEmpty(final RawDocument jsonDocument) {
        final String value = jsonDocument.getValue();
        if (value.trim().isEmpty()) {
            return errorResult("Document is empty");
        }
        return okResult(jsonDocument);
    }

    private ValidationMessage getError(JsonParseException exception) {
        final String message = exception.getMessage();
        for (final Map.Entry<Pattern, Function<JsonParseException, ValidationMessage>> entry : errorMapper.entrySet()) {
            final boolean matches = entry.getKey().asPredicate().test(message);
            if (matches) {
                return entry.getValue().apply(exception);
            }
        }
        return defaultError(exception);
    }

    private ValidationMessage defaultError(final JsonParseException exception) {
        return jsonError(exception.getLocation(), exception.getMessage());
    }

    private ValidationMessage unclosedObject(final JsonParseException exception) {
        return jsonError(exception.getLocation(), "Missing close bracket [}]");
    }

    private ValidationMessage unopenedObject(final JsonParseException exception) {
        final String description = "Close bracket [}] found but not matching open bracket [{] was found";
        return jsonError(exception.getLocation(), description);
    }

    private ValidationMessage missingStartQuoteOnPropertyName(final JsonParseException exception) {
        return jsonError(exception.getLocation(), "Property names must start with quote [\"]");
    }

    private ValidationMessage missingEndQuoteOnPropertyName(final JsonParseException exception) {
        return jsonError(exception.getLocation(), "Missing closing quote [\"] in property name");
    }

    private ValidationMessage emptyPropertyName(final JsonParseException exception) {
        return jsonError(exception.getLocation(), "Property names cannot be empty");
    }

    private ValidationMessage missingColon(final JsonParseException exception) {
        final String description = "Missing colon [:] to separate property name from value";
        return jsonError(exception.getLocation(), description);
    }

    private ValidationMessage invalidPropertyValue(final JsonParseException exception) {
        final String description = "Invalid property value. Valid values are strings enclosed in quotes [\"], [true], [false], [null], [NaN] and numbers";
        return jsonError(exception.getLocation(), description);
    }

    private ValidationMessage missingEndQuoteOnPropertyValue(final JsonParseException exception) {
        final String description = "Missing closing quote [\"] on string property value";
        return jsonError(exception.getLocation(), description);
    }

    private ValidationMessage missingPropertyValue(final JsonParseException exception) {
        final String description = "Property value is missing";
        return jsonError(exception.getLocation(), description);
    }

    private ValidationMessage unclosedArray(final JsonParseException exception) {
        return jsonError(exception.getLocation(), "Array is not closed");
    }

    private ValidationMessage redundantCommaInArray(final JsonParseException exception) {
        final String description = "Array is closed but a trailing comma [,] does not separate a value";
        return jsonError(exception.getLocation(), description);
    }
}
