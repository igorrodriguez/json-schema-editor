package com.daslernen.jsonschemaeditor.validation;

import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

import static com.daslernen.jsonschemaeditor.validation.ValidationMessage.error;
import static com.daslernen.jsonschemaeditor.validation.ValidationMessage.ok;
import static com.daslernen.jsonschemaeditor.validation.ValidationStatus.ERROR;
import static java.lang.String.format;

final class ValidationVisitor<T> {

    private static final Logger LOG = LoggerFactory.getLogger(ValidationVisitor.class);
    private static final Comparator<ValidationMessage> MESSAGE_LEVEL_COMPARATOR = (m1, m2) -> m1.getStatus().compareTo(m2.getStatus());

    private final Result<T> result;

    public static <T> ValidationVisitor<T> create(final Supplier<Result<T>> action) {
        return createSafe(action);
    }

    private ValidationVisitor(final Result<T> result) {
        Preconditions.checkNotNull(result);
        this.result = result;
    }

    public <U> ValidationVisitor<U> map(final Function<T, Result<U>> action) {
        if (result.message.getStatus() == ERROR) {
            return new ValidationVisitor<>(Result.errorResult(result.message.getText()));
        }
        return createSafe(() -> action.apply(result.value));
    }

    public <U, V> ValidationVisitor<V> merge(final ValidationVisitor<U> visitor, final BiFunction<T, U, V> mapper) {
        return new ValidationVisitor<>(calculateResult(visitor, mapper));
    }

    private <V, U> Result<V> calculateResult(final ValidationVisitor<U> visitor, final BiFunction<T, U, V> mapper) {
        final ValidationMessage visitorMessage = visitor.getMessage();
        if (getMessage().getStatus() == ERROR) {
            return Result.result(getMessage());
        } else if (visitorMessage.getStatus() == ERROR) {
            return Result.result(visitorMessage);
        }
        return mergeSafe(visitor, mapper);
    }

    private <V, U> Result<V> mergeSafe(ValidationVisitor<U> visitor, BiFunction<T, U, V> mapper) {
        final T thisValue = this.result.value;
        final U visitorValue = visitor.result.value;
        try {
            final V mergedValue = mapper.apply(thisValue, visitorValue);
            final ValidationMessage visitorMessage = visitor.getMessage();
            final int result = MESSAGE_LEVEL_COMPARATOR.compare(getMessage(), visitorMessage);
            final ValidationMessage message = result > 0 ? getMessage() : visitorMessage;
            return Result.result(message, mergedValue);
        } catch (Exception e) {
            return Result.errorResult(format("Failed to combine [%s] with [%s]", thisValue, visitorValue));
        }
    }

    public Result<T> getResult() {
        return result;
    }

    public ValidationMessage getMessage() {
        return result.getMessage();
    }

    private static <T> ValidationVisitor<T> createSafe(final Supplier<Result<T>> action) {
        final Result<T> newResult;
        try {
            newResult = action.get();
        } catch (Exception e) {
            final String errorMessage = format("Internal error caused by [%s]", e);
            LOG.error(errorMessage, e);
            return new ValidationVisitor<>(Result.errorResult(errorMessage));
        }
        return new ValidationVisitor<>(newResult);
    }

    static final class Result<T> {

        private final ValidationMessage message;
        private final T value;

        public static <T> Result<T> okResult(final T value) {
            return result(ok(), value);
        }

        static <T> Result<T> result(final ValidationMessage message) {
            return result(message, null);
        }

        public static <T> Result<T> result(final ValidationMessage message, final T value) {
            return new Result<>(message, value);
        }

        public static <T> Result<T> errorResult(final String messageText) {
            return result(error(messageText), null);
        }

        private Result(final ValidationMessage message, final T value) {
            Preconditions.checkNotNull(message);
            this.message = message;
            this.value = value;
        }

        public ValidationMessage getMessage() {
            return message;
        }

        public Optional<T> getValue() {
            return Optional.ofNullable(value);
        }
    }

    static final class None { }
}
