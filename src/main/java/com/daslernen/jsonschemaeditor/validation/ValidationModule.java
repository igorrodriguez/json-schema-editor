package com.daslernen.jsonschemaeditor.validation;

import com.daslernen.jsonschemaeditor.annotation.Json;
import com.daslernen.jsonschemaeditor.annotation.Schema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.google.inject.AbstractModule;

public final class ValidationModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(Validator.class).annotatedWith(Json.class).to(JsonValidator.class);
        bind(Validator.class).annotatedWith(Schema.class).to(SchemaValidator.class);
        bind(JsonSchemaValidator.class).to(SchemaValidator.class);
        bind(JsonSchemaFactory.class).toInstance(JsonSchemaFactory.byDefault());
    }
}
