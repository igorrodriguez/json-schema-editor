package com.daslernen.jsonschemaeditor.validation;

public interface Validator {

    ValidationMessage validate(RawDocument value);
}
