package com.daslernen.jsonschemaeditor.service;

import javaslang.control.Try;

import java.nio.file.Path;

public interface FileService {

    boolean exists(Path file);

    Try<String> readString(Path path);

    Try<Path> save(Path path, String fileContent);
}
