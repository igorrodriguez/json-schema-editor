package com.daslernen.jsonschemaeditor.service;

import javaslang.control.Try;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Files;
import java.nio.file.Path;

import static java.nio.charset.StandardCharsets.UTF_8;

public final class DefaultFileService implements FileService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultFileService.class);

    @Override
    public boolean exists(Path file) {
        return Files.exists(file);
    }

    @Override
    public Try<String> readString(Path path) {
        LOG.debug("Loading file [{}]", path);
        return Try.of(() -> new String(Files.readAllBytes(path), UTF_8))
                .onFailure(e -> LOG.error("Failed to open file", e));
    }

    @Override
    public Try<Path> save(Path path, String fileContent) {
        LOG.debug("Saving file [{}]", path);
        return Try.of(() -> Files.write(path, fileContent.getBytes(UTF_8)))
                .onFailure(e -> LOG.error("Failed to save file", e));
    }
}
