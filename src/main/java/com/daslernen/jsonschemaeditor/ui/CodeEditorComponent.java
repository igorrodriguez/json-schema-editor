package com.daslernen.jsonschemaeditor.ui;

import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import org.fxmisc.flowless.Virtualized;

import java.util.function.Consumer;

interface CodeEditorComponent {

    <T extends Node & Virtualized> T getNode();

    void addTextChangeListener(Consumer<String> listener);

    String getText();

    ObservableValue<Integer> currentParagraphProperty();

    ObservableValue<Integer> caretColumnProperty();

    void replaceText(String s);

    void moveCaretToStartOfDocument();
}
