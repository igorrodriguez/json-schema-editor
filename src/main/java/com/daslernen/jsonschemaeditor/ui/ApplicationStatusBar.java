package com.daslernen.jsonschemaeditor.ui;

import com.daslernen.jsonschemaeditor.domain.PercentageDoubleStringConverter;
import com.daslernen.jsonschemaeditor.domain.PercentageStringConverter;
import javafx.scene.control.Slider;
import javafx.scene.control.Tooltip;

import javax.inject.Inject;

final class ApplicationStatusBar extends EditorStatusBar {

    private final PercentageDoubleStringConverter doubleConverter;
    private final PercentageStringConverter stringConverter;

    @Inject
    public ApplicationStatusBar(final PercentageDoubleStringConverter doubleConverter,
                                final PercentageStringConverter stringConverter,
                                final ImageLoader imageLoader) {
        super(imageLoader);
        this.doubleConverter = doubleConverter;
        this.stringConverter = stringConverter;

        final Slider scaleSlider = createScaleSlider();
        scaleSlider.valueProperty().bindBidirectional(scaleProperty());
        getNode().getRightItems().add(scaleSlider);
    }

    private Slider createScaleSlider() {
        final Slider slider = new Slider(100.0, 400.0, 100.0);
        slider.setShowTickLabels(true);
        slider.setBlockIncrement(100.0);
        slider.setSnapToTicks(true);
        slider.setMajorTickUnit(100.0);
        slider.setLabelFormatter(doubleConverter);
        slider.setPrefWidth(300);
        final Tooltip tooltip = new Tooltip();
        tooltip.textProperty().bindBidirectional(slider.valueProperty(), stringConverter);
        slider.setTooltip(tooltip);
        return slider;
    }
}
