package com.daslernen.jsonschemaeditor.ui.highlight;

import com.google.common.base.Preconditions;
import javaslang.collection.List;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.stream.Collectors;

final class RegexBuilder {

    private final List<RegexNode> regexNodes;
    private final String groupName;

    public static RegexBuilder find(final String regexString) {
        Preconditions.checkNotNull(regexString);

        return new RegexBuilder(null, List.of(new DefaultRegexNode(regexString, "")));
    }

    private RegexBuilder(final String groupName, final List<RegexNode> regexNodes) {
        this.groupName = groupName;
        this.regexNodes = regexNodes;
    }

    public RegexBuilder or(final String regexString) {
        return new RegexBuilder(groupName, regexNodes.append(new DefaultRegexNode(regexString, "|")));
    }

    public RegexBuilder captureTrailingWhiteSpaces() {
        final List<RegexNode> wrappedNodes = this.regexNodes.map(TrailingWhiteSpaceRegexNode::new);
        return new RegexBuilder(groupName, wrappedNodes);
    }

    public RegexBuilder group(final String name) {
        return new RegexBuilder(name, regexNodes);
    }

    public String getGroupName() {
        return groupName;
    }

    public String build() {
        return wrapWithGroup(regexNodes.toJavaStream()
                .map(RegexNode::create)
                .collect(Collectors.joining("")));
    }

    private String wrapWithGroup(final String regex) {
        if (groupName == null) {
            return regex;
        }
        return String.format("(?<%s>(%s))", groupName, regex);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("regexNodes", regexNodes)
                .toString();
    }

    private interface RegexNode {
        String create();
    }

    private static final class TrailingWhiteSpaceRegexNode implements RegexNode {

        private final RegexNode original;

        TrailingWhiteSpaceRegexNode(final RegexNode original) {
            Preconditions.checkNotNull(original);
            this.original = original;
        }

        @Override
        public String create() {
            return original.create() + "\\s*";
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("original", original)
                    .toString();
        }
    }

    private static final class DefaultRegexNode implements RegexNode {

        private final String value;
        private final String operator;

        DefaultRegexNode(String value, String operator) {
            Preconditions.checkNotNull(value);
            Preconditions.checkNotNull(operator);

            this.value = value;
            this.operator = operator;
        }

        @Override
        public String create() {
            return operator + value;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("value", value)
                    .append("operator", operator)
                    .toString();
        }
    }
}
