package com.daslernen.jsonschemaeditor.ui.highlight;

import javaslang.collection.List;
import org.fxmisc.richtext.StyleSpans;
import org.fxmisc.richtext.StyleSpansBuilder;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.daslernen.jsonschemaeditor.ui.highlight.RegexBuilder.find;
import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;

public class JsonHighlighter {

    private static final RegexBuilder PUNCTUATION_MARK_REGEX = find("\\{").or("\\}").or("\\[").or("\\]").or(",").or(":").group("punctuation");
    /** Any text enclosed in quotes that is followed by a colon */
    private static final RegexBuilder PROPERTY_REGEX = find("\"[^\"]*+\"(?=\\s?:)").group("property");
    /** Any text enclosed in quotes that is NOT followed by a colon */
    private static final RegexBuilder STRING_REGEX = find("\"[^\"]*+\"(?!\\s?:)").group("string");
    private static final RegexBuilder NUMBER_REGEX = find("\\d+").or("[\\d+\\.\\d+]").group("number");
    private static final RegexBuilder PROPERTY_VALUE_REGEX = find("null").or("true").or("false").group("propertyvalue");

    /**
     * A map where keys are both the name of the matching group and the name of the style in the CSS file, and the values are the pattern
     * used to identify the strings that belong to such a group.
     */
    private static final List<RegexBuilder> STYLE_PATTERNS = List
            .of(PUNCTUATION_MARK_REGEX, PROPERTY_REGEX, STRING_REGEX, NUMBER_REGEX, PROPERTY_VALUE_REGEX);

    private final Pattern pattern;

    public JsonHighlighter() {
        final String patternString = STYLE_PATTERNS
                .map(regexBuilder -> regexBuilder.captureTrailingWhiteSpaces().build())
                .toJavaStream()
                .collect(Collectors.joining("|"));
        this.pattern = Pattern.compile(patternString);
    }

    public StyleSpans<Set<String>> computeHighlighting(final String text) {
        final StyleSpansBuilder<Set<String>> spansBuilder = new StyleSpansBuilder<>();
        final Matcher matcher = pattern.matcher(text);
        int lastKeywordEnd = 0;
        while (matcher.find()) {
            final String styleClass = getStyleClass(matcher);
            spansBuilder.add(emptySet(), matcher.start() - lastKeywordEnd);
            spansBuilder.add(singleton(styleClass), matcher.end() - matcher.start());
            lastKeywordEnd = matcher.end();
        }
        spansBuilder.add(emptySet(), text.length() - lastKeywordEnd);
        return spansBuilder.create();
    }

    public String getStylesheetPath() {
        return getClass().getResource("json-keywords.css").toExternalForm();
    }

    private String getStyleClass(final Matcher matcher) {
        for (final RegexBuilder regexBuilder : STYLE_PATTERNS) {
            final String styleName = regexBuilder.getGroupName();
            if (matcher.group(styleName) != null) {
                return styleName;
            }
        }
        throw new IllegalStateException("No group found");
    }
}
