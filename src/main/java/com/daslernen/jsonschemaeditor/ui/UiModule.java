package com.daslernen.jsonschemaeditor.ui;

import com.daslernen.jsonschemaeditor.annotation.Json;
import com.daslernen.jsonschemaeditor.annotation.Schema;
import com.daslernen.jsonschemaeditor.configuration.EditorConfiguration;
import com.daslernen.jsonschemaeditor.domain.DialogProvider;
import com.daslernen.jsonschemaeditor.domain.DocumentHandler;
import com.daslernen.jsonschemaeditor.validation.Validator;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import javafx.stage.Stage;

public final class UiModule extends AbstractModule {

    private final Stage stage;

    public UiModule(final Stage stage) {
        this.stage = stage;
    }

    @Override
    protected void configure() {
        bind(Stage.class).toInstance(stage);
        bind(DialogProvider.class).to(FxDialogProvider.class);
    }

    @Json
    @Provides
    public CodeEditorComponent createJsonCodeEditor(@Json final EditorConfiguration editorConfiguration) {
        return new CodeEditor(editorConfiguration);
    }

    @Schema
    @Provides
    public CodeEditorComponent createSchemaCodeEditor(@Schema final EditorConfiguration editorConfiguration) {
        return new CodeEditor(editorConfiguration);
    }

    @Json
    @Provides
    public EditorToolbar createJsonEditorToolbar(@Json final EditorConfiguration configuration, final ImageLoader imageLoader) {
        return new EditorToolbar(configuration, imageLoader);
    }

    @Schema
    @Provides
    public EditorToolbar createSchemaEditorToolbar(@Schema final EditorConfiguration configuration, final ImageLoader imageLoader) {
        return new EditorToolbar(configuration, imageLoader);
    }

    @Json
    @Provides
    public EditorPane createJsonEditor(@Json final EditorConfiguration configuration,
                                       @Json final Validator validator,
                                       @Json final DocumentHandler documentHandler,
                                       @Json final EditorToolbar toolbar,
                                       @Json final CodeEditorComponent codeEditor,
                                       final EditorStatusBar statusBar) {
        return new EditorPane(configuration, validator, documentHandler, toolbar, codeEditor, statusBar);
    }

    @Schema
    @Provides
    public EditorPane createSchemaEditor(@Schema final EditorConfiguration configuration,
                                         @Schema final Validator validator,
                                         @Schema final DocumentHandler documentHandler,
                                         @Schema final EditorToolbar toolbar,
                                         @Schema final CodeEditorComponent codeEditor,
                                         final EditorStatusBar statusBar) {
        return new EditorPane(configuration, validator, documentHandler, toolbar, codeEditor, statusBar);
    }
}
