package com.daslernen.jsonschemaeditor.ui;

import com.daslernen.jsonschemaeditor.configuration.WindowProperties;
import javafx.collections.ObservableList;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.Comparator;

import static java.lang.String.format;

public final class WindowPositionManager {

    private static final Logger LOG = LoggerFactory.getLogger(WindowPositionManager.class);
    private static final int MIN_VISIBLE_POSITION = 100;

    private final WindowProperties properties;

    @Inject
    public WindowPositionManager(final WindowProperties properties) {
        this.properties = properties;
    }

    void configure(final Stage stage) {
        final ObservableList<Screen> screens = Screen.getScreens();
        LOG.debug("Screens: {}", screens);

        final double x = Math.min(properties.getX(), getMaxX(screens) - MIN_VISIBLE_POSITION);
        stage.setX(x);
        stage.setY(Math.min(properties.getY(), getY(x, screens) - MIN_VISIBLE_POSITION));
        stage.setWidth(properties.getWidth());
        stage.setHeight(properties.getHeight());
        properties.xProperty().bind(stage.xProperty());
        properties.yProperty().bind(stage.yProperty());
        properties.widthProperty().bind(stage.widthProperty());
        properties.heightProperty().bind(stage.heightProperty());
    }

    private double getMaxX(final ObservableList<Screen> screens) {
        return screens.stream()
                .map(screen -> screen.getBounds().getMaxX())
                .max(Comparator.naturalOrder())
                .orElseThrow(() -> new IllegalStateException("No screen found!"));
    }

    private double getY(final double x, final ObservableList<Screen> screens) {
        return screens.stream()
                .map(Screen::getBounds)
                .filter(bounds -> bounds.getMinX() <= x && bounds.getMaxX() > x)
                .map(Rectangle2D::getMaxY)
                .max(Comparator.naturalOrder())
                .orElseThrow(() -> new IllegalStateException(format("No screen found for x=[%s]", x)));
    }
}
