package com.daslernen.jsonschemaeditor.ui;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

abstract class ScalableComponent implements Component {

    private static final double DEFAULT_IMAGE_SIZE = 100;

    private final DoubleProperty scale = new SimpleDoubleProperty(DEFAULT_IMAGE_SIZE);

    final DoubleProperty scaleProperty() {
        return scale;
    }
}
