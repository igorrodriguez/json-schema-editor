package com.daslernen.jsonschemaeditor.ui;

import com.daslernen.jsonschemaeditor.annotation.Json;
import com.daslernen.jsonschemaeditor.annotation.Schema;
import com.daslernen.jsonschemaeditor.configuration.WindowProperties;
import com.daslernen.jsonschemaeditor.validation.JsonSchemaValidator;
import com.daslernen.jsonschemaeditor.validation.RawDocument;
import com.daslernen.jsonschemaeditor.validation.ValidationMessage;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;

import javax.inject.Inject;

final class RootPane {

    private static final int DEFAULT_SIZE = 100;

    private final double scaleFactor = DEFAULT_SIZE / Font.getDefault().getSize();

    private final WindowProperties windowProperties;
    private final JsonSchemaValidator jsonSchemaValidator;
    private final EditorPane jsonEditor;
    private final EditorPane schemaEditor;
    private final ApplicationStatusBar statusBar;

    @Inject
    public RootPane(final WindowProperties windowProperties,
                    final JsonSchemaValidator jsonSchemaValidator,
                    @Json EditorPane jsonEditor,
                    @Schema EditorPane schemaEditor,
                    final ApplicationStatusBar statusBar) {
        this.windowProperties = windowProperties;
        this.jsonSchemaValidator = jsonSchemaValidator;
        this.jsonEditor = jsonEditor;
        this.schemaEditor = schemaEditor;
        this.statusBar = statusBar;
    }

    BorderPane getPane() {
        jsonEditor.addTextChangeListener(json -> validateJsonAgainstSchema(json, schemaEditor.getDocument()));
        schemaEditor.addTextChangeListener(schema -> validateJsonAgainstSchema(jsonEditor.getDocument(), schema));

        jsonEditor.init();
        schemaEditor.init();

        final SplitPane rootPane = new SplitPane(jsonEditor, schemaEditor);
        final BorderPane root = new BorderPane();
        root.setCenter(rootPane);
        root.setBottom(statusBar.getNode());
        bindScaleSlider(root, jsonEditor, schemaEditor);

        return root;
    }

    private void bindScaleSlider(final BorderPane root, EditorPane jsonEditor, EditorPane schemaEditor) {
        final DoubleProperty scaleProperty = windowProperties.scaleProperty();
        jsonEditor.scaleProperty().bindBidirectional(scaleProperty);
        schemaEditor.scaleProperty().bindBidirectional(scaleProperty);
        statusBar.scaleProperty().bindBidirectional(scaleProperty);
        root.styleProperty().bind(Bindings.concat("-fx-font-size: ", statusBar.scaleProperty().divide(scaleFactor).asString(), ";"));
    }

    private void validateJsonAgainstSchema(final RawDocument jsonDocument, final RawDocument schemaDocument) {
        final ValidationMessage message = jsonSchemaValidator.validateAgainstSchema(jsonDocument, schemaDocument);
        statusBar.setMessage(message);
    }
}
