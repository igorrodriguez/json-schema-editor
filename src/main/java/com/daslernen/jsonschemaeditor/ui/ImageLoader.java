package com.daslernen.jsonschemaeditor.ui;

import javafx.beans.property.DoubleProperty;
import javafx.scene.image.ImageView;

final class ImageLoader {

    private static final double DEFAULT_SCALE_FACTOR = 4;

    ImageView loadImage(final String path, final DoubleProperty scaleProperty) {
    final ImageView image = new ImageView(path);
    image.fitHeightProperty().bind(scaleProperty.divide(DEFAULT_SCALE_FACTOR));
    image.fitWidthProperty().bind(scaleProperty.divide(DEFAULT_SCALE_FACTOR));
    image.setPreserveRatio(true);
    image.setSmooth(true);
    image.setCache(true);
    return image;
}
}
