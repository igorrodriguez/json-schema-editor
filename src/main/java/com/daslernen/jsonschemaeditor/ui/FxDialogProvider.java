package com.daslernen.jsonschemaeditor.ui;

import com.daslernen.jsonschemaeditor.domain.DialogProvider;
import com.daslernen.jsonschemaeditor.domain.SaveConfirmation;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.controlsfx.dialog.ExceptionDialog;

import javax.inject.Inject;
import java.io.File;
import java.nio.file.Path;
import java.util.Map;
import java.util.Optional;

import static com.daslernen.jsonschemaeditor.domain.SaveConfirmation.*;

final class FxDialogProvider implements DialogProvider {

    private static final Map<ButtonType, SaveConfirmation> saveConfirmationMap = ImmutableMap.<ButtonType, SaveConfirmation>builder()
            .put(ButtonType.YES, SAVE)
            .put(ButtonType.NO, DO_NOT_SAVE)
            .put(ButtonType.CANCEL, CANCEL)
            .build();

    private final Stage owner;
    private final FileChooser fileChooser;

    @Inject
    FxDialogProvider(final Stage owner) {
        Preconditions.checkNotNull(owner);

        this.owner = owner;
        fileChooser = new FileChooser();
    }

    @Override
    public Optional<Path> showOpenDialog(final Path initialDirectory) {
        fileChooser.setInitialDirectory(initialDirectory.toFile());
        return Optional.ofNullable(fileChooser.showOpenDialog(owner))
                .map(File::toPath);
    }

    @Override
    public Optional<Path> showSaveDialog(final Path initialDirectory) {
        fileChooser.setInitialDirectory(initialDirectory.toFile());
        return Optional.ofNullable(fileChooser.showSaveDialog(owner))
                .map(File::toPath);
    }

    @Override
    public void showErrorAlert(final String title, final String message, final Throwable exception) {
        final ExceptionDialog dialog = new ExceptionDialog(exception);
        dialog.initOwner(owner);
        dialog.setTitle(title);
        dialog.setHeaderText(message);
        dialog.showAndWait();
    }

    @Override
    public SaveConfirmation showSaveConfirmationAlert(final String title, final String message) {
        final Alert alert = new Alert(Alert.AlertType.CONFIRMATION, message, ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
        alert.setHeaderText(title);
        return alert.showAndWait()
                .map(saveConfirmationMap::get)
                .orElseThrow(() -> new IllegalStateException("Unexpected response from alert"));
    }
}
