package com.daslernen.jsonschemaeditor.ui;

import javafx.scene.Node;

interface Component {

    Node getNode();
}
