package com.daslernen.jsonschemaeditor.ui;

import com.daslernen.jsonschemaeditor.validation.ValidationMessage;
import com.daslernen.jsonschemaeditor.validation.ValidationStatus;
import com.google.common.collect.ImmutableMap;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import org.controlsfx.control.StatusBar;

import javax.inject.Inject;
import java.util.Map;

import static java.lang.String.format;

class EditorStatusBar extends ScalableComponent {

    private final StatusBar statusBar = new StatusBar();
    private final Map<ValidationStatus, ImageView> imageMapper;
    private final StringProperty caretPositionProperty;

    @Inject
    EditorStatusBar(final ImageLoader imageLoader) {
        final ImageView okImage = imageLoader.loadImage("icons/ballgreen.png", scaleProperty());
        final ImageView warningImage = imageLoader.loadImage("icons/ballorange.png", scaleProperty());
        final ImageView errorImage = imageLoader.loadImage("icons/ballred.png", scaleProperty());
        imageMapper = ImmutableMap.<ValidationStatus, ImageView>builder()
                .put(ValidationStatus.OK, okImage)
                .put(ValidationStatus.WARNING, warningImage)
                .put(ValidationStatus.ERROR, errorImage)
                .build();

        final Label caretPositionLabel = createCaretPositionLabel();
        statusBar.getRightItems().add(new BorderPane(caretPositionLabel));
        caretPositionProperty = caretPositionLabel.textProperty();
        statusBar.setText("");
    }

    public void setMessage(final ValidationMessage message) {
        statusBar.setGraphic(getIcon(message.getStatus()));
        statusBar.setText(message.getText());
    }

    @Override
    public StatusBar getNode() {
        return statusBar;
    }

    StringProperty caretPositionPropertyProperty() {
        return caretPositionProperty;
    }

    private ImageView getIcon(final ValidationStatus status) {
        final ImageView icon = imageMapper.get(status);
        if (icon == null) {
            throw new IllegalArgumentException(format("Icon not found for status [%s]", status));
        }
        return icon;
    }

    private Label createCaretPositionLabel() {
        final Label label = new Label();
        label.setTooltip(new Tooltip("Current position of the caret in the document in the format 'line:character'"));
        return label;
    }
}
