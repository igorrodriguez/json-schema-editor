package com.daslernen.jsonschemaeditor.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class Configuration {

    @JsonProperty
    private WindowProperties windowProperties = new WindowProperties();

    @JsonProperty
    private EditorConfiguration jsonEditor = new EditorConfiguration();

    @JsonProperty
    private EditorConfiguration schemaEditor = new EditorConfiguration();

    public EditorConfiguration getJsonEditor() {
        return jsonEditor;
    }

    public Configuration setJsonEditor(EditorConfiguration jsonEditor) {
        this.jsonEditor = jsonEditor;
        return this;
    }

    public EditorConfiguration getSchemaEditor() {
        return schemaEditor;
    }

    public Configuration setSchemaEditor(EditorConfiguration schemaEditor) {
        this.schemaEditor = schemaEditor;
        return this;
    }

    public WindowProperties getWindowProperties() {
        return windowProperties;
    }

    public Configuration setWindowProperties(WindowProperties windowProperties) {
        this.windowProperties = windowProperties;
        return this;
    }
}
