package com.daslernen.jsonschemaeditor.configuration;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.nio.file.Path;
import java.nio.file.Paths;

public final class EditorConfiguration {

    private String title;
    private Path lastOpenDirectory = Paths.get("/");
    private OpenFile openFile = new OpenFile();
    private final StringProperty pathToRoot = new SimpleStringProperty("");

    public String getTitle() {
        return title;
    }

    public EditorConfiguration setTitle(String title) {
        this.title = title;
        return this;
    }

    public Path getLastOpenDirectory() {
        return lastOpenDirectory;
    }

    public EditorConfiguration setLastOpenDirectory(Path lastOpenDirectory) {
        this.lastOpenDirectory = lastOpenDirectory;
        return this;
    }

    public OpenFile getOpenFile() {
        return openFile;
    }

    public EditorConfiguration setOpenFile(OpenFile openFile) {
        this.openFile = openFile;
        return this;
    }

    public String getPathToRoot() {
        return pathToRoot.getValue();
    }

    public EditorConfiguration setPathToRoot(String pathToRoot) {
        this.pathToRoot.setValue(pathToRoot);
        return this;
    }

    public StringProperty pathToRootProperty() {
        return pathToRoot;
    }
}
