package com.daslernen.jsonschemaeditor.configuration;

import com.daslernen.jsonschemaeditor.domain.NewLineStyle;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.nio.file.Path;
import java.util.Optional;

public final class OpenFile {

    private static final int DEFAULT_FONT_SIZE = 14;

    private final ObjectProperty<Path> path = new SimpleObjectProperty<>();
    private String content;
    private NewLineStyle newLineStyle = NewLineStyle.SYSTEM_DEFAULT_STYLE;
    private final DoubleProperty fontSize = new SimpleDoubleProperty(DEFAULT_FONT_SIZE);

    public Optional<Path> getPath() {
        return Optional.ofNullable(path.get());
    }

    public ObjectProperty<Path> pathProperty() {
        return path;
    }

    public OpenFile setPath(Path path) {
        this.path.set(path);
        return this;
    }

    public Optional<String> getContent() {
        return Optional.ofNullable(content);
    }

    public OpenFile setContent(String content) {
        this.content = content;
        return this;
    }

    public NewLineStyle getNewLineStyle() {
        return newLineStyle;
    }

    public OpenFile setNewLineStyle(NewLineStyle newLineStyle) {
        this.newLineStyle = newLineStyle;
        return this;
    }

    public double getFontSize() {
        return fontSize.get();
    }

    public DoubleProperty fontSizeProperty() {
        return fontSize;
    }

    public OpenFile setFontSize(double fontSize) {
        this.fontSize.set(fontSize);
        return this;
    }
}
