package com.daslernen.jsonschemaeditor.configuration;

import com.daslernen.jsonschemaeditor.annotation.Json;
import com.daslernen.jsonschemaeditor.annotation.Schema;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

public final class ConfigurationModule extends AbstractModule {

    @Override
    protected void configure() {
    }

    @Provides
    @Singleton
    public Configuration createConfiguration(final ConfigurationLoader configurationLoader) {
        return configurationLoader.load();
    }

    @Provides
    @Singleton
    public WindowProperties getWindowProperties(final Configuration configuration) {
        return configuration.getWindowProperties();
    }

    @Json
    @Provides
    @Singleton
    public EditorConfiguration getJsonEditorConfiguration(final Configuration configuration) {
        return configuration.getJsonEditor();
    }

    @Schema
    @Provides
    @Singleton
    public EditorConfiguration getSchemaEditorConfiguration(final Configuration configuration) {
        return configuration.getSchemaEditor();
    }
}
