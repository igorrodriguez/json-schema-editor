package com.daslernen.jsonschemaeditor.ui.highlight

import spock.lang.Specification

class RegexBuilderTest extends Specification {

    def "find creates a regex that is equal to the passed argument"() {
        when:
        def regex = RegexBuilder.find("\\d").build()

        then:
        regex == "\\d"
    }

    def "concatenating with 'or' creates a regex that separates elements with '|'s"() {
        given:
        def regexBuilder = RegexBuilder.find("aaa")

        when:
        def regex = regexBuilder.or("bbb").build()

        then:
        regex == "aaa|bbb"
    }

    def "building with 'captureTrailingWhiteSpaces' modifies every module to capture trailing white spaces"() {
        given:
        def regexBuilder = RegexBuilder.find("aaa")

        when:
        def regex = regexBuilder.or("bbb").captureTrailingWhiteSpaces().build()

        then:
        regex == "aaa\\s*|bbb\\s*"
    }

    def "calling 'group' wraps the regex in a named group"() {
        given:
        def regexBuilder = RegexBuilder.find("aaa")
                .or("bbb")
                .captureTrailingWhiteSpaces()

        when:
        def regex = regexBuilder.group("mygroup").build()

        then:
        regex == "(?<mygroup>(aaa\\s*|bbb\\s*))"
    }

    def "calling 'group' before 'captureTrailingWhiteSpaces' has the same effect that doing it after"() {
        given:
        def regexBuilder = RegexBuilder.find("aaa")
                .or("bbb")

        when:
        def regex = regexBuilder
                .group("mygroup")
                .captureTrailingWhiteSpaces()
                .build()

        then:
        regex == "(?<mygroup>(aaa\\s*|bbb\\s*))"
    }
}
