package com.daslernen.jsonschemaeditor.domain

import spock.lang.Specification

import java.nio.file.Paths
import java.util.function.Consumer

class DocumentStateHolderTest extends Specification {

    def "new instances contain a default state"() {
        when:
        def defaultState = new DocumentStateHolder().getState()

        then:
        defaultState == CurrentFile.NONE
    }

    def "cannot update state by passing a null path"() {
        given:
        def underTest = new DocumentStateHolder()

        when:
        underTest.updateState(Paths.get("/some/path"), null)

        then:
        thrown(NullPointerException)
    }

    def "cannot update state by passing a null content"() {
        given:
        def underTest = new DocumentStateHolder()

        when:
        underTest.updateState(null, "some-content")

        then:
        thrown(NullPointerException)
    }

    def "updating state changes the state and notifies listeners with the new value"() {
        given:
        def newPath = Paths.get("/some/path")
        def newContent = "some-content"
        def underTest = new DocumentStateHolder()
        Consumer<CurrentFile> listener = Mock()
        underTest.addStateChangeListener(listener)

        when:
        underTest.updateState(newPath, newContent)

        then:
        def expectedState = new CurrentFile(newPath, newContent)
        underTest.getState() == expectedState
        listener.accept(expectedState)
    }

    def "updateToNone changes the state and notifies listeners with the new value"() {
        given:
        def underTest = new DocumentStateHolder()
        underTest.updateState(Paths.get("/some/path"), "some-content")
        Consumer<CurrentFile> listener = Mock()
        underTest.addStateChangeListener(listener)

        when:
        underTest.updateToNone()

        then:
        underTest.getState() == CurrentFile.NONE
        listener.accept(CurrentFile.NONE)
    }

    def "listeners are not notified if the new state is equal to the previous one"() {
        given:
        def underTest = new DocumentStateHolder()
        Consumer<CurrentFile> listener = Mock()
        underTest.addStateChangeListener(listener)

        when:
        underTest.updateToNone()

        then:
        underTest.getState() == CurrentFile.NONE
        0 * listener.accept(_)
    }
}
