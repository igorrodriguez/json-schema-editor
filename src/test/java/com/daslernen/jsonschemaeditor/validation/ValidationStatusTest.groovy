package com.daslernen.jsonschemaeditor.validation

import spock.lang.Specification

import static com.daslernen.jsonschemaeditor.validation.ValidationStatus.*

class ValidationStatusTest extends Specification {

    def "comparator sorts error > warning > info"() {
        given:
        def statuses = [WARNING, OK, ERROR, ERROR, OK, WARNING]

        when:
        def sortedStatuses = statuses.toSorted(descendingLogLevelComparator())

        then:
        sortedStatuses == [ERROR, ERROR, WARNING, WARNING, OK, OK]
    }
}
