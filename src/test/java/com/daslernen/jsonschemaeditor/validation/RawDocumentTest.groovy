package com.daslernen.jsonschemaeditor.validation

import spock.lang.Specification

class RawDocumentTest extends Specification {

    def "an instance created without specifying the path to root gets the default one"() {
        when:
        def document = new RawDocument("a-value")

        then:
        document.getValue() == "a-value"
        document.getPathToRoot() == ""
    }

    def "cannot create an instance with null value"() {
        when:
        new RawDocument(null, "some-path-to-root")

        then:
        thrown NullPointerException
    }

    def "cannot create an instance with null path to root"() {
        when:
        new RawDocument("a-value", null)

        then:
        thrown NullPointerException
    }
}
