package com.daslernen.jsonschemaeditor.validation

import com.daslernen.jsonschemaeditor.domain.JsonMapper
import spock.lang.Specification
import spock.lang.Unroll

class JsonValidatorTest extends Specification {

    private JsonValidator underTest

    def setup() {
        underTest = new JsonValidator(new JsonMapper())
    }

    @Unroll
    def "no validation error is returned"() {
        when:
        def message = underTest.validate(new RawDocument(jsonValue))

        then:
        message.status == ValidationStatus.OK

        where:
        jsonValue     | _
        "{}"          | _
        """{"a":1}""" | _
    }

    @Unroll
    def "one validation error is returned"() {
        when:
        def message = underTest.validate(new RawDocument(jsonValue))

        then:
        message.status == ValidationStatus.ERROR
        message.text.contains(errorMessage)

        where:
        jsonValue                 | errorMessage
        ""                        | "Document is empty"
        """{"""                   | "Missing close bracket [}]"
        """}"""                   | "Close bracket [}] found but not matching open bracket [{] was found"
        """{abc": 1}"""           | "Property names must start with quote [\"]"
        """{"abc: 1}"""           | "Missing closing quote [\"] in property name"
        """{"abc" 1}"""           | "Missing colon [:] to separate property name from value"
        """{"abc": w}"""          | "Invalid property value. Valid values are strings enclosed in quotes [\"], [true], [false], [null], [NaN] and numbers"
        """{"abc": tru}"""        | "Invalid property value. Valid values are strings enclosed in quotes [\"], [true], [false], [null], [NaN] and numbers"
        """{"":{"values": [)}}""" | "Invalid property value. Valid values are strings enclosed in quotes [\"], [true], [false], [null], [NaN] and numbers"
        """{"abc":}"""            | "Property value is missing"
        """{"abc": "w}"""         | "Missing closing quote [\"] on string property value"
        """{"abc": [ 1,2}"""      | "Array is not closed"
        """{"abc": [ 1,]}"""      | "Array is closed but a trailing comma [,] does not separate a value"
        "{\"\""                   | "Property names cannot be empty"
    }

    def "path to root must exist in the document"() {
        given:
        def pathToRoot = "/missing"
        def json = """
        {
          "firstName" : "test"
        }
        """

        when:
        def message = underTest.validate(new RawDocument(json, pathToRoot))

        then:
        message.status == ValidationStatus.ERROR
        message.text == "Path [/missing] doesn't point to a valid json"
    }

    def "validateJson returns the json node in the path to root of the document"() {
        given:
        def pathToRoot = "/test"
        def json = """
        {
          "test" : {
            "firstName" : "test"
          }
        }
        """

        when:
        def message = underTest.validate(new RawDocument(json, pathToRoot))

        then:
        message.status == ValidationStatus.OK
    }
}
