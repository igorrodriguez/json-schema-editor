package com.daslernen.jsonschemaeditor.validation

import com.fasterxml.jackson.core.JsonPointer
import spock.lang.Specification

class DocumentTest extends Specification {

    def "single argument constructor creates a document with an empty path to root"() {
        when:
        def document = new Document("some-value")

        then:
        document.value == "some-value"
        document.pathToRoot.toString() == ""
    }

    def "cannot create an instance with a null value"() {
        when:
        new Document(null, JsonPointer.compile(""))

        then:
        thrown NullPointerException
    }

    def "cannot create an instance with a null json pointer"() {
        when:
        new Document("some-value", null)

        then:
        thrown NullPointerException
    }

    def "isValidPath returns true when the passed path is valid"() {
        when:
        def isValid = Document.isValidPath("/some/valid/path")

        then:
        isValid
    }

    def "isValidPath returns false when the passed path is invalid"() {
        when:
        def isValid = Document.isValidPath("invalid/path")

        then:
        !isValid
    }
}
