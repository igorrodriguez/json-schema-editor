package com.daslernen.jsonschemaeditor.validation

import com.fasterxml.jackson.core.JsonLocation
import com.google.common.testing.EqualsTester
import junit.framework.AssertionFailedError
import spock.lang.Specification

import static com.daslernen.jsonschemaeditor.validation.ValidationStatus.*

class ValidationMessageTest extends Specification {

    def "ok() messages have OK status and 'Valid' message"() {
        when:
        def validationMessage = ValidationMessage.ok()

        then:
        validationMessage.status == OK
        validationMessage.text == "Valid"
    }

    def "warn(message) messages have WARNING status and the message passed as argument"() {
        when:
        def validationMessage = ValidationMessage.warn("some-error")

        then:
        validationMessage.status == WARNING
        validationMessage.text == "some-error"
    }

    def "null values are not accepted as argument for warn(message)"() {
        when:
        ValidationMessage.warn(null)

        then:
        thrown NullPointerException
    }

    def "error(message) messages have ERROR status and the message passed as argument"() {
        when:
        def validationMessage = ValidationMessage.error("some-error")

        then:
        validationMessage.status == ERROR
        validationMessage.text == "some-error"
    }

    def "null values are not accepted as argument for error(message)"() {
        when:
        ValidationMessage.error(null)

        then:
        thrown NullPointerException
    }

    def "null values are not accepted as arguments for jsonError(location, message)"() {
        when:
        ValidationMessage.jsonError(location, message)

        then:
        thrown NullPointerException

        where:
        location                            | message
        null                                | "some-error"
        new JsonLocation(null, 10L, 22, 33) | null
    }

    def "jsonError() messages have ERROR status and a message generated with both passed parameters"() {
        when:
        def validationMessage = ValidationMessage.jsonError(new JsonLocation(null, 10L, 22, 33), "some-error")

        then:
        validationMessage.status == ERROR
        validationMessage.text == "(22:33) some-error"
    }

    def "equals and hashcode"() {
        when:
        new EqualsTester()
                .addEqualityGroup(new ValidationMessage(OK, "some-message"), new ValidationMessage(OK, "some-message"))
                .addEqualityGroup(new ValidationMessage(WARNING, "some-message"), new ValidationMessage(WARNING, "some-message"))
                .addEqualityGroup(new ValidationMessage(OK, "another-message"), new ValidationMessage(OK, "another-message"))
                .testEquals()
        then:
        notThrown(AssertionFailedError)
    }
}
