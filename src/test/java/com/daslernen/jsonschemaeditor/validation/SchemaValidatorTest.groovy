package com.daslernen.jsonschemaeditor.validation

import com.daslernen.jsonschemaeditor.domain.JsonMapper
import com.github.fge.jsonschema.main.JsonSchemaFactory
import spock.lang.Specification

class SchemaValidatorTest extends Specification {

    def underTest = new SchemaValidator(new JsonMapper(), JsonSchemaFactory.byDefault(), new SchemaMessageCreator())

    def "an empty or whitespace string is not a valid schema"() {
        given:
        def schema = "   "

        when:
        def message = underTest.validate(new RawDocument(schema))

        then:
        message.status == ValidationStatus.ERROR
        message.text == "Document is empty"
    }

    def "when the json of the schema is invalid an error is returned"() {
        given:
        def schema = """
        {
          "properties" : {  // Invalid json
            "firstName" : {
              "type" : "string"
            }
          }
        }
        """

        when:
        def message = underTest.validate(new RawDocument(schema))

        then:
        message.status == ValidationStatus.ERROR
        message.text.contains("Unexpected character ('/'")
    }

    def "schema's path to root must exist in its json"() {
        given:
        def pathToRoot = "/missing"
        def schema = """
        {
          "properties" : {
            "firstName" : {
              "type" : "string"
            }
          }
        }
        """

        when:
        def message = underTest.validate(new RawDocument(schema, pathToRoot))

        then:
        message.status == ValidationStatus.ERROR
        message.text == "Path [/missing] doesn't point to a valid json"
    }

    def "validating an schema with an 'invalid' property type returns an error"() {
        given:
        def schema = """
        {
          "type": "object",
          "properties": {
            "firstName": {
              "type": "invalid"
            }
          },
          "required": ["firstName"]
        }
        """

        when:
        def message = underTest.validate(new RawDocument(schema))

        then:
        message.status == ValidationStatus.ERROR
        message.text == """[/properties/firstName] "invalid" is not a valid primitive type (valid values are: [array, boolean, integer, null, number, object, string])"""
    }

    def "a valid schema with not path to root returns a successful message"() {
        given:
        def schema = """
        {
          "properties" : {
            "firstName" : {
              "type" : "string"
            }
          }
        }
        """

        when:
        def message = underTest.validate(new RawDocument(schema))

        then:
        message.status == ValidationStatus.OK
    }

    def "a valid schema with a valid path to root returns a successful message"() {
        given:
        def pathToRoot = "/test"
        def schema = """
        {
          "test" : {
            "properties" : {
              "firstName" : {
                "type" : "string"
              }
            }
          }
        }
        """

        when:
        def message = underTest.validate(new RawDocument(schema, pathToRoot))

        then:
        message.status == ValidationStatus.OK
    }

    // validateAgainstSchema

    def validJsonDocument = new RawDocument("""{ "firstName": "test" }""")
    def validSchemaDocument = new RawDocument(
            """
            {
              "properties" : {
                "firstName" : {
                  "type" : "string"
                }
              }
            }
            """)

    def "an empty json cannot be validated against a schema"() {
        given:
        def json = "   "

        when:
        def message = underTest.validateAgainstSchema(new RawDocument(json), validSchemaDocument)

        then:
        message.status == ValidationStatus.ERROR
        message.text == "Document is empty"
    }

    def "json syntax error are shown directly"() {
        given:
        def invalidJson = """{ "firstName": "test", }"""
        def schema = """
        {
          "properties" : {
            "firstName" : {
              "type" : "string"
            }
          }
        }
        """

        when:
        def message = underTest.validateAgainstSchema(new RawDocument(invalidJson), new RawDocument(schema))

        then:
        message.status == ValidationStatus.ERROR
        message.text == """(1:25) Property names must start with quote ["]"""
    }

    def "path to root of json must exist to allow it to be validated"() {
        given:
        def pathToRoot = "/missing"
        def json = """
        {
          "firstName" : "test"
        }
        """

        when:
        def message = underTest.validateAgainstSchema(new RawDocument(json, pathToRoot), validSchemaDocument)

        then:
        message.status == ValidationStatus.ERROR
        message.text == "Path [/missing] doesn't point to a valid json"
    }

    def "an empty or whitespace string is not a valid schema to validate against"() {
        given:
        def emptySchema = "   "

        when:
        def message = underTest.validateAgainstSchema(validJsonDocument, new RawDocument(emptySchema))

        then:
        message.status == ValidationStatus.ERROR
        message.text == "Document is empty"
    }

    def "validateAgainstSchema(): when the json of the schema is invalid an error is returned"() {
        given:
        def schema = """
        {
          "properties" : {  // Invalid json
            "firstName" : {
              "type" : "string"
            }
          }
        }
        """

        when:
        def message = underTest.validateAgainstSchema(validJsonDocument, new RawDocument(schema))

        then:
        message.status == ValidationStatus.ERROR
        message.text.contains("Unexpected character ('/'")
    }

    def "validateAgainstSchema(): schema's path to root must exist in its json"() {
        given:
        def pathToRoot = "/missing"
        def schema = """
        {
          "properties" : {
            "firstName" : {
              "type" : "string"
            }
          }
        }
        """

        when:
        def message = underTest.validateAgainstSchema(validJsonDocument, new RawDocument(schema, pathToRoot))

        then:
        message.status == ValidationStatus.ERROR
        message.text == "Path [/missing] doesn't point to a valid json"
    }

    def "validateAgainstSchema(): validating an schema with an 'invalid' property type returns an error"() {
        given:
        def schema = """
        {
          "type": "object",
          "properties": {
            "firstName": {
              "type": "invalid"
            }
          }
        }
        """

        when:
        def message = underTest.validateAgainstSchema(validJsonDocument, new RawDocument(schema))

        then:
        message.status == ValidationStatus.ERROR
        message.text == """[/properties/firstName] "invalid" is not a valid primitive type (valid values are: [array, boolean, integer, null, number, object, string])"""
    }

    def "validateAgainstSchema(): a valid schema with not path to root returns a successful message"() {
        given:
        def schema = """
        {
          "properties" : {
            "firstName" : {
              "type" : "string"
            }
          }
        }
        """

        when:
        def message = underTest.validateAgainstSchema(validJsonDocument, new RawDocument(schema))

        then:
        message.status == ValidationStatus.OK
    }

    def "validateAgainstSchema(): a valid schema with a valid path to root returns a successful message for a json with no root"() {
        given:
        def pathToRoot = "/test"
        def schema = """
        {
          "test" : {
            "properties" : {
              "firstName" : {
                "type" : "string"
              }
            }
          }
        }
        """

        when:
        def message = underTest.validateAgainstSchema(validJsonDocument, new RawDocument(schema, pathToRoot))

        then:
        message.status == ValidationStatus.OK
    }

    def "validateAgainstSchema(): a valid schema with a valid path to root returns a successful message for a json with valid root"() {
        given:
        def jsonPathToRoot = "/json-test"
        def json ="""
        {
          "json-test" : {
            "firstName" : "test"
          }
        }
        """
        def schemaPathToRoot = "/schema-test"
        def schema = """
        {
          "schema-test" : {
            "properties" : {
              "firstName" : {
                "type" : "string"
              }
            }
          }
        }
        """

        when:
        def message = underTest.validateAgainstSchema(new RawDocument(json, jsonPathToRoot), new RawDocument(schema, schemaPathToRoot))

        then:
        message.status == ValidationStatus.OK
    }
}
